package com.epam.shape;

public class Circle extends Shape implements Color{

    @Override
    void figureShape() {
        System.out.println("Circle has round shape");
    }

    @Override
    public void draw() {
        System.out.println("Circles are "+rd+" and " + bl +" and "+ gn);
    }

}
