package com.epam.shape;

import java.util.ArrayList;

public interface Color {
    String bl = "BLUE";
    String gn = "GREEN";
    String rd = "RED";
    String yw = "YELLOW";
    String og = "ORANGE";
    String prp = "PURPLE";
    String gr = "GREY";

    void draw();

}

