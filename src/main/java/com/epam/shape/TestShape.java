package com.epam.shape;

public class TestShape {
    public static void main(String[] args) {
        String corners[] = {"Circle has no corners", "Triangle has 3 corners", "Rectangle has 4 corners"};

            System.out.println(corners[0]);
            Circle circle = new Circle();
            circle.figureShape();
            circle.draw();
            System.out.println("---------------------------------");
            System.out.println(corners[1]);
            Triangle triangle = new Triangle();
            triangle.figureShape();
            triangle.draw();
            System.out.println("---------------------------------");
            System.out.println(corners[2]);
            Rectangle rectangle = new Rectangle();
            rectangle.figureShape();
            rectangle.draw();
            System.out.println("---------------------------------");
        }

    }



