package com.epam.zoo;

public class Test {
    public static void main(String[] args) {
        Animal animal=new Animal();
        animal.Animal();
        animal.eatsAnything();
        animal.sleeps();
        System.out.println("--------------------------------");
        Bird bird = new Bird();
        bird.Birds();
        bird.eatsAnything();
        bird.sleeps();
        bird.flies();
        bird.callSound();
        System.out.println("---------------------------------");
        Fish fish = new Fish();
        fish.eatsAnything();
        fish.sleeps();
        fish.swim();
        System.out.println("---------------------------------");
        Parrot parrot=new Parrot();
        parrot.Parrot();
        parrot.flies();
        parrot.sleeps();
        parrot.eatPlants();
        parrot.callSound();
        System.out.println("---------------------------------");
        Eagle eagle=new Eagle();
        eagle.Eagle();
        eagle.flies();
        eagle.sleeps();
        eagle.eatsMeat();
        System.out.println("---------------------------------");
   }

}
