package com.epam.zoo;

class Fish extends Animal {
    public Fish() {
        super();
        System.out.println("A new fish has been created!");
        // TODO Auto-generated constructor stub
    }
    public void sleeps() {
        System.out.println("A fish sleeps...");
    }
    public void eatsAnything() {
        System.out.println("A fish eats anything...");
    }
    public void swim(){
        System.out.println("A fish swims...");
    }
}
