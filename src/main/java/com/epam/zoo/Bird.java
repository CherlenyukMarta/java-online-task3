package com.epam.zoo;

class Bird extends Animal implements IMakeSound {
    public void Birds() {
        System.out.println("A new bird has been created!");
        // TODO Auto-generated constructor stub
    }

    public void sleeps() {
        System.out.println("A bird sleeps...");
    }
    public void eatsAnything() {
        System.out.println("A bird eats anything...");
    }
    public  void flies(){
        System.out.println("A bird flies...");
    }

    @Override
    public void callSound() {
    System.out.println("I make sound: 'cvirin-cvirin'");
    }
}

