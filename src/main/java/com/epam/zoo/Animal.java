package com.epam.zoo;

public class Animal {
    public void Animal() {
        System.out.println("A new animal has been created!");
    }
    public void sleeps() {
        System.out.println("An animal sleeps...");
    }
    public void eatsAnything() {
        System.out.println("An animal eats anything...");
    }
}
