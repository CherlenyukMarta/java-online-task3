package com.epam.zoo;

    class Parrot extends Bird implements IMakeSound {

        public void Parrot() {
            System.out.println("A new parrot has been created!");
        }

        public void sleeps() {
            System.out.println("A parrot sleeps...");
        }
        public void flies(){
            System.out.println("A parrot flies...");
        }

        public void eatPlants(){
        System.out.println("A parrot eats plants...");
    }

        @Override
        public void callSound() {
        System.out.println("I make sound: 'Riki-tiki..''");
        }
    }
