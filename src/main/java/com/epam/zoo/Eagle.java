package com.epam.zoo;

public class Eagle extends Bird {
    public void Eagle(){
        System.out.println("A new eagle has been created!");
    }
    public void sleeps() {
        System.out.println("An eagle sleeps...");
    }
    public void flies(){
        System.out.println("An eagle flies...");
    }
    public void eatsMeat(){
        System.out.println("An eagle eats meat...");
    }
}
